#!/bin/sh

echo "TOKEN_AUTH=${TOKEN_AUTH}" >> /etc/crontab
echo "*/3 * * * * root /script.sh &> /var/log/cron/cron.log" >> /etc/crontab

# start cron
/usr/sbin/crond -n