#!/bin/sh

# code goes here.
python /matomo-log-analytics/import_logs.py --url=http://test-matomo-api.web.cern.ch --debug --recorders=4 --enable-http-errors --enable-http-redirects --enable-static --enable-bots --token-auth=${TOKEN_AUTH} `date --date=today +/data/%Y/%m/%Y-%m-%d-access.log`