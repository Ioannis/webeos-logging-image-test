FROM cern/cc7-base

RUN yum -y update && \
    yum install -y git && \
    git clone https://github.com/matomo-org/matomo-log-analytics && \
    yum clean all -y

COPY import_logs.py /matomo-log-analytics/
COPY cron-launcher.sh /
ADD script.sh /script.sh

RUN chmod +x /script.sh /cron-launcher.sh && \
    mkdir /data && \
    mkdir /var/log/cron/ && touch /var/log/cron/cron.log

ENTRYPOINT ["/cron-launcher.sh"]